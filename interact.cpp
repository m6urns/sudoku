/***********************************************************************
* Program:
*    Project 12, Mad Lib          (e.g. Assignment 01, Hello World)
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary:
*    This is a partial compleation of the mid way point of the 
* project 12. I have not yet been able to accomplish editing.
*
*    Estimated:  5.0 hrs
*    Actual:     8.0 hrs
*      Printing the output correctly formatted was difficult
*      
************************************************************************/

#include <iostream>
#include <fstream>

using namespace std;

void menu();
void getFileName(char fileName[]);
void loadBoard(int row, char column, int value, 
               char input, char fileName[], int board[][9]);
void displayBoard(int board[][9]);
void getEdit(int row, char column, int value, int board[][9]);
void editBoard(int row, char column, int value, int board[][9]);
void getCompute(int row, char column, int value, int board[][9]);
void computeValues(int row, char column, int value, int board[][9]);
void saveBoard(int board[][9]);
void prompt(int row, char column, int value,
            char input, char fileName[], int board[][9]);

int main() {
   
   char input;
   char fileName[256];
   int board[9][9];

   int row;
   char column;
   int value = 0;

   loadBoard(row, column, value, input, fileName, board);
   menu();
   displayBoard(board);
   prompt(row, column, value, input, fileName, board);

   return 0;
}

void prompt(int row, char column, int value, 
            char input, char fileName[], int board[][9]) {

   cout << endl;
   cout << "> ";

   cin >> input;
   
   input = toupper(input);

   switch (input)
   {
      case '?':
         menu();
         prompt(row, column, value, input, fileName, board);
         break;
      case ('D'):
         displayBoard(board);
         prompt(row, column, value, input, fileName, board);
         break;
      case 'E':
         getEdit(row, column, value, board);
         prompt(row, column, value, input, fileName, board);
         break;
      case 'S':
         getCompute(row, column, value, board);
         prompt(row, column, value, input, fileName, board);
         break;
      case 'Q':
         saveBoard(board);
         break;
      case 'L':
         loadBoard(row, column, value, input, fileName, board);
         menu();
         prompt(row, column, value, input, fileName, board);
         break;
      default:
         cout << "Error not a valid command." << endl;
         prompt(row, column, value, input, fileName, board);
         break;    
   }     
}

void menu() {
   
   cout << "Options:" << endl;
   cout << "   ?  Show these instructions" << endl;
   cout << "   D  Display the board" << endl;
   cout << "   E  Edit one square" << endl;
   cout << "   S  Show the possible values for a square" << endl;
   cout << "   Q  Save and Quit" << endl;

   return;
}

void getFileName(char fileName[]) {
      
   cout << "Where is your board located? ";
   cin.getline(fileName, 256);

   return;
}

void loadBoard(int row, char column, int value, 
               char input, char fileName[], int board[][9]) {

   getFileName(fileName);

   ifstream fin(fileName);

   if (fin.fail())
   {
      menu();
      prompt(row, column, value, input, fileName, board);
      return;
   }
   
   int zero = 0;

   for (int i = 0; i < 9; i++)
   {
      board[i][zero] = 0;
      zero++;
   }

   int r = 0;
   int c = 0;
     
   while (fin >> board[r][c])
   {
      c++;

      if (c == 9)
      {
         c = 0;
         r++;
      }     
   } 

   fin.close();
   
   return;
}

void displayBoard(int board[][9]) {
   
   cout << endl;

	int row = 0;
	int col = 0;

   cout << "   A B C D E F G H I" << endl;

   for (int i = 0; i < 89; i++)
   {
      if (board[row][col] == 0)
      {
         switch (col)
         {
            case 0:
               cout << (row + 1) << "  " << " ";
               col++;
               break;
            case 1:
               cout << "  ";            
               col++;
               break;
            case 2:
               cout << "  ";            
               col++;
               break;
            case 3:
               cout << "| ";            
               col++;
               break;
            case 4:
               cout << "  ";
               col++;
               break;
            case 5:
               cout << "  ";
               col++;
               break;
            case 6:
               cout << "| ";
               col++;
               break;
            case 7:
               cout << "  ";
               col++;
               break;
            case 8:
               cout << "  ";
               col++;
               break;
            case 9:
               if (row == 2)
               {
                  cout << endl;
                  cout << "   -----+-----+-----" << endl;
               }
               else if (row == 5)
               {
                  cout << endl;
                  cout << "   -----+-----+-----" << endl;
               }
               else
                  cout << endl;
            
               row++;
               col = 0;
               break;
         }
      }
     
      else
      {
         switch (col)
         {
            case 0:
               cout << (row + 1) << "  " << board[row][col];
               col++;
               break;
            case 1:
               cout << " " << board[row][col];            
               col++;
               break;
            case 2:
               cout << " " << board[row][col];            
               col++;
               break;
            case 3:
               cout << "|" << board[row][col];            
               col++;
               break;
            case 4:
               cout << " " << board[row][col];
               col++;
               break;
            case 5:
               cout << " " << board[row][col];
               col++;
               break;
            case 6:
               cout << "|" << board[row][col];
               col++;
               break;
            case 7:
               cout << " " << board[row][col];
               col++;
               break;
            case 8:
               cout << " " << board[row][col];
               col++;
               break;
            case 9:
               if (row == 2)
               {
                  cout << endl;
                  cout << "   -----+-----+-----" << endl;
               }
               else if (row == 5)
               {
                  cout << endl;
                  cout << "   -----+-----+-----" << endl;
               }
               else
                  cout << endl;
            
            row++;
            col = 0;
            break;
         }
      }
   }

   cout << endl;

   return;
}

void getEdit(int row, char column, int value, int board[][9]) {

   cout << "What are the coordinates of the square: ";
   
   cin >> column;
   cin >> row;

   if (cin.fail())
   {
      cin.clear();
      cin.ignore(256, '\n');

      cout << "Invalid coordinates.";
   }

   column = toupper(column);

   if (row < 1 || row > 9)
   {
      cout << "Invalid row." << endl;
   }
   
   if (column < 'A' || column > 'I')
   {
      cout << "Invalid column." << endl;
   }
 
   editBoard(row, column, value, board);
    
   return;
}

void editBoard(int row, char column, int value, int board[][9]) {

   int rowActual = (row - 1);
   int columnActual;

   if (column == 'A')
      columnActual = 0;
   else if (column == 'B')
      columnActual = 1;
   else if (column == 'C')
      columnActual = 2;
   else if (column == 'D')
      columnActual = 3;
   else if (column == 'E')
      columnActual = 4;
   else if (column == 'F')
      columnActual = 5;
   else if (column == 'G')
      columnActual = 6;
   else if (column == 'H')
      columnActual = 7;
   else if (column == 'I')
      columnActual = 8;
   else
   {
      cout << "Not a valid column" << endl;
      return;
   }
   
   if (board[rowActual][columnActual] != 0)
   {
      cout << "ERROR: Square \'" << column << row << "\' " << "is filled" << endl;
      return;
   }
   else
   cout << "What is the value at \'" << column << row << "\': ";
   cin >> value;

   board[rowActual][columnActual] = value;
             
   return;
}

void getCompute(int row, char column, int value, int board[][9]) {

   cout << "What are the coordinates of the square: ";
   
   cin >> column;
   cin >> row;

   if (cin.fail())
   {
      cin.clear();
      cin.ignore(256, '\n');

      cout << "Invalid coordinates.";
   }

   column = toupper(column);

   if (row < 1 || row > 9)
   {
      cout << "Invalid row." << endl;
   }
   
   if (column < 'A' || column > 'I')
   {
      cout << "Invalid column." << endl;
   }
 
   computeValues(row, column, value, board);
    
   return;
}

void computeValues(int row, char column, int value, int board[][9]) {
 
   int rowActual = (row - 1);
   int columnActual;

   if (column == 'A')
      columnActual = 0;
   else if (column == 'B')
      columnActual = 1;
   else if (column == 'C')
      columnActual = 2;
   else if (column == 'D')
      columnActual = 3;
   else if (column == 'E')
      columnActual = 4;
   else if (column == 'F')
      columnActual = 5;
   else if (column == 'G')
      columnActual = 6;
   else if (column == 'H')
      columnActual = 7;
   else if (column == 'I')
      columnActual = 8;
   else
   {
      cout << "Not a valid column" << endl;
      return;
   }


   value = board[rowActual][columnActual];

   int rowValues[9];
   int columnValues[9];
   int boxValues[9];

   for (int i = 0; i < 9; i++)
   {
         rowValues[i] = board[rowActual][i];
         columnValues[i] = board[i][columnActual];
   }
   
   //cout << "Row " << " Column " << " Box" << endl;

   for (int i = 0; i < 9; i++)
   {
    //  cout << rowValues[i] << "    " << columnValues[i] << endl;
   }
   
   
   int values[18] = {0};
   
   for (int row = 0; row < 9; row++)
   {
      if (rowValues[row] != 0)
         values[row] = rowValues[row];
   }
   
   for (int col = 9; col < 18; col++)
   {
      if (columnValues[col] != 0)
         values[col] = columnValues[col - 9];
   }
   
   for (int i = 0; i < 18; i++)
   {
      if (values[i] == 0)
      {
         int z = 0;
         z++;
      }
      else
      {
         cout << values[i] << " ";
      }
   }
   
   //Print possibles
   cout << endl;
   
   for (int i = 1; i <= 9; i++)
   {
	   if (values[i] != i)
		cout << i << " ";
	}

   return;
}

void saveBoard(int board[][9]) {
 
   char saveName[256];

   int row = 0;
   int col = 0;

   cin.ignore();

   cout << "What file would you like to write your board to: ";
   cin.getline(saveName, 256);

   ofstream fout(saveName);
   
   if (fout.fail())
      return;

   for (int i = 0; i < 89; i++)
   {
      switch (col)
      {
         case 0:
            fout << board[row][col];
            col++;
            break;
         case 1:
            fout << " " << board[row][col];            
            col++;
            break;
         case 2:
            fout << " " << board[row][col];            
            col++;
            break;
         case 3:
            fout << " " << board[row][col];            
            col++;
            break;
         case 4:
            fout << " " << board[row][col];
            col++;
            break;
         case 5:
            fout << " " << board[row][col];
            col++;
            break;
         case 6:
            fout << " " << board[row][col];
            col++;
            break;
         case 7:
            fout << " " << board[row][col];
            col++;
            break;
         case 8:
            fout << " " << board[row][col];
            col++;
            break;
         case 9:
            fout << endl;
            col = 0;
            row++;
            break;
         }  
   }

   fout.close();

   return;
}
