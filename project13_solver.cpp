/***********************************************************************
* Program:
*    Project Final, Mad Lib          (e.g. Assignment 01, Hello World)
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary:
*    This program reads a sudoku file the user can specify, it will
* 	 print the file and allow the user to edit it, this version
*    has an integrated solver.
*
*    Estimated:  8.0 hrs
*    Actual:     18.0 hrs
*      Find possible solutions for a specified place on the grid was 
*      by far the most difficult, I thought that was significantly 
*      more difficult than the solver.
************************************************************************/

#include <iostream>
#include <fstream>

using namespace std;

#define UNASSIGNED 0
#define N 9

bool findUnassignedLocation(int grid[N][N], int &row, int &col);
bool isSafe(int grid[N][N], int row, int col, int num);
bool computeValues(string loc, int grid[N][N], int checkVal = 0);
string promptPos(int grid[N][N]);
void interact(int grid[N][N]);

/***********************************************************************
 * The void menu prints out the menu for at the first run and if the 
 * user asks the program to reprint it.
 ***********************************************************************/
void menu() {
   
   cout << "Options:" << endl;
   cout << "   ?  Show these instructions" << endl;
   cout << "   D  Display the board" << endl;
   cout << "   E  Edit one square" << endl;
   cout << "   S  Show the possible values for a square" << endl;
   cout << "   X  Solve the puzzle" << endl;
   cout << "   Q  Save and Quit" << endl;
   cout << endl;

   return;
}

/***********************************************************************
 * The funtion solve recurses across a grid and attempts to fill in
 * the grid following the rules of sudoku. 
 ***********************************************************************/
bool solve(int grid[N][N])
{
    int row, col;
    
    if (!findUnassignedLocation(grid, row, col))
       return true;
       
    for (int num = 1; num <= 9; num++)
    {
		if (isSafe(grid, row, col, num))
        {
			grid[row][col] = num;
			
			if (solve(grid))
                return true;
                
            grid[row][col] = UNASSIGNED;
            
        }
    }
    
    return false;
 }
 
/***********************************************************************
 * This bool searchs the grid trying to find a entry which is unassigned.
 * If it is found the location will be set and the bool will return 
 * true.
 ***********************************************************************/
bool findUnassignedLocation(int grid[N][N], int &row, int &col)
{
    for (row = 0; row < N; row++)
        for (col = 0; col < N; col++)
            if (grid[row][col] == UNASSIGNED)
                return true;
    return false;
}
 
/***********************************************************************
 * The bool usedInRow shows whether an assigned entry from the find un
 * assigned location function to decide if the row matches the given
 * number.
 ***********************************************************************/
bool usedInRow(int grid[N][N], int row, int num)
{
    for (int col = 0; col < N; col++)
        if (grid[row][col] == num)
            return true;
    return false;
}

/***********************************************************************
 * Same as row, but for column.
 ***********************************************************************/
bool usedInCol(int grid[N][N], int col, int num)
{
    for (int row = 0; row < N; row++)
        if (grid[row][col] == num)
            return true;
    return false;
}

/***********************************************************************
 * Same a row and column but searchs through a box to find values which 
 * match the assigned value. If it doesn't it returns true.
 ***********************************************************************/
bool usedInBox(int grid[N][N], int boxStartRow, int boxStartCol, int num)
{
    for (int row = 0; row < 3; row++)
        for (int col = 0; col < 3; col++)
            if (grid[row+boxStartRow][col+boxStartCol] == num)
                return true;
    return false;
}

/***********************************************************************
 * Checks a value against all three bool for usedInBox, usedInRow, and
 * in usedInCol.
 ***********************************************************************/
bool isSafe(int grid[N][N], int row, int col, int num)
{
	 return !usedInRow(grid, row, num) &&
           !usedInCol(grid, col, num) &&
           !usedInBox(grid, row - row%3 , col - col%3, num);
}

/***********************************************************************
 * The void getFileName was by far the easiest part of this program
 * it passes the char fileName back to main where it is used to 
 * load a file.
 ***********************************************************************/
void getFileName(char fileName[]) {
      
   cout << "Where is your board located? ";
   cin.getline(fileName, 256);

   return;
}

/***********************************************************************
 * The void save grid saves the grid to a user specified file, and
 * informs them if the write is good. Other wise it fails and asks the
 * user to try again.
 ***********************************************************************/
void saveGrid(int grid[N][N])
{
   char saveName[256];
   
   cin.ignore();
   
   cout << "What file would you like to write your board to: ";
   cin.getline(saveName, 256);
   
   ofstream fout(saveName);
   
   if (fout.fail())
   {
      cout << "Failed to save file." << endl;
      interact(grid);
   }
   
   for (int r = 0; r < 9; r++)
   {
      for (int c = 0; c < 9; c++)
      {
            if (c == 0)
               fout << grid[r][c];
            else
               fout << " " << grid[r][c];
         
         if (c == 8)
            continue;
      }
   fout << endl;
   }
   
   cout << "Board written successfully" << endl;
}

/***********************************************************************
 * The void loadboard loads the board into the the in array so it can be
 * modified by the user, this program does not have highlighting, despite 
 * the fact it could be easily done by overlaying two arrays.
 ***********************************************************************/
void loadBoard(int grid[N][N], char fileName[]) 
{

   getFileName(fileName);

   ifstream fin(fileName);
   
   if (fin.fail())
   {
	   menu();
	   interact(grid);
	   return;
   }
   
   int r = 0;
   int c = 0;
     
   while (fin >> grid[r][c])
   {
      c++;
      if (c == 9)
      {
         c = 0;
         r++;
      }     
   } 

   fin.close();
   
   return;
}

/***********************************************************************
 * The void edit grid is resposible for editng the gird, it also checks
 * to assure the user is inputing a valid number into the array.
 ***********************************************************************/
void editGrid(string pos, int grid[][9])
{
   if (pos == "")
      return;

   int val;
   cout << "What is the value at '" << pos << "': ";
   cin >> val;

   if (val > 9 || val < 1 || !(computeValues(pos, grid, val)))
   {
      cout << "ERROR: Value '" << val << "' in square '"
           << pos << "' is invalid\n";

      return;
  }

   int row = pos[1] - '1';
   int col = pos[0] - (isupper(pos[0]) ? 'A' : 'a');

   grid[row][col] = val;

   return;
}

/***********************************************************************
 * The string prompt pos asks the user for a postion, it returns this
 * position in th form of a sting for the function computeValues 
 * to use to check the validity of a users input.
 ***********************************************************************/
string promptPos(int grid[N][N])
{
   string pos;
   cout << "What are the coordinates of the square: ";
   cin >> pos;

   int row = pos[1] - '1';
   int col = pos[0] - (isupper(pos[0]) ? 'A' : 'a');

   if (!isalpha(pos[0]) || (!isdigit(pos[1]) || pos.length() > 2)
       || row + 1 > 9 || row + 1 < 1 || col + 1 > 9 || col + 1 < 1)
   {
      cout << "ERROR: Square '" << pos << "' is invalid\n";
      return "";
   }

   if (grid[row][col] > 0)
   {
      cout << "ERROR: Square '" << pos << "' is filled\n";
      return "";
   }

   return pos;
}

/***********************************************************************
 * The bool compute values will either show the users possible values
 * for a place on the grid, or it is used to validate user input in the
 * edit function.
 ***********************************************************************/
bool computeValues(string loc, int grid[N][N], int checkVal)
{
   if (loc ==  "")
      return false;

   short numBooleans = 0b111111111;
   int row = loc[1] - '1';
   int col = loc[0] - (isupper(loc[0]) ? 'A' : 'a');
   int blockR = row / 3;
   int blockC = col / 3;

   for (int c = 0; c < 9; c++)
      if (grid[row][c] > 0)
         numBooleans = numBooleans & ~(1 << (grid[row][c] - 1));

   for (int r = 0; r < 9; r++)
      if (grid[r][col] > 0)
         numBooleans = numBooleans & ~(1 << (grid[r][col] - 1));

   for (int r = blockR * 3; r < blockR * 3 + 3; r++)
      for (int c = blockC * 3; c < blockC * 3 + 3; c++)
         if (grid[r][c] > 0)
            numBooleans = numBooleans & ~(1 << (grid[r][c] - 1));

   if (checkVal == 0)
   {
      cout << "The possible values for '" << loc << "' are: ";
      int count = 0;
      for (int i = 0; i < 9; i++)
         if (numBooleans & (1 << i))
         {
            if (count > 0)
               cout << ", ";
            cout << i + 1;
            count++;
         }
      cout << endl;
   }
   else
      return numBooleans & (1 << (checkVal - 1));

   cout << endl;
   interact(grid);

   return false;
}

/***********************************************************************
 * The void printgrid will print the grid for the user, is will
 * also format the output very nicely.
 ***********************************************************************/
void printGrid(int grid[N][N])
{
	cout << "   A B C D E F G H I" << endl;

   for (int r = 0; r < 9; r++)
   {
      cout << (r + 1) << "  ";
      for (int c = 0; c < 9; c++)
      {
         if (grid[r][c] != 0)
            cout << grid[r][c];
         else
            cout << " ";
         if (c == 8)
            continue;
         cout << ((c + 1) % 3 == 0 ? '|' : ' ');
      }
      cout << endl;
 
      if (r != 8 && (r + 1) % 3 == 0)
         cout << "   -----+-----+-----\n";
   }
   
   cout << endl;
   interact(grid);
}

/***********************************************************************
 * The void interact is resposible for interacting with the user, it 
 * checks what the user has input against a case statement and takes
 * action based on the users input.
 ***********************************************************************/
void interact(int grid[N][N]) 
{
   char input;
   
   cout << "> ";
   cin >> input;  
 
   input = toupper(input);
   
   switch (input)
   {
      case '?':
         menu();
         cout << endl;
         interact(grid);
         break;
      case 'D':
         printGrid(grid);
         break;
      case 'E':
         editGrid(promptPos(grid), grid);
         cout << endl;
         interact(grid);
         break;
      case 'S':
         computeValues(promptPos(grid), grid);
         break;
      case 'Q':
         saveGrid(grid);
         break;
      case 'X':
		 if (solve(grid) == true)
			printGrid(grid);
		 else 
			cout << "No solution" << endl;
		 break;
      default:
         cout << "Error not a valid command." << endl;
         interact(grid);
         break;
   }
}

/***********************************************************************
 * The function main holds a couple variables and initaites the program 
 * for the user, after that all it does is hold the array for the user.
 ***********************************************************************/
int main()
{
    char fileName[256];
    int grid[N][N]; 
    
    loadBoard(grid, fileName);
    menu();
    printGrid(grid);
    
    return 0;
}
