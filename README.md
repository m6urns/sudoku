Project 4: Sudoku

Timeline:

- Interface design
- Read file and print 
- Locate on array
- push changes to file

- Add option to specify file in command line + command
  line solver
  
- This program can solve any sudoku board
- I did not bother myself with syntax highlighting or
	robust error checking... sorry

Design Documentation:

Function Layout:

                                  main()
                                    |
                                    |
                                interact()
									|
							   handleOption()
                                    |   
       ------------------------------------------------
      display()       editSquare()      showSquare()  |
                                                 ------------                               
                                               save()      read()  


      Utility Functions:
         int computeValue()
         bool isOpen()
		 void readFile()
		      getPossible()
			  showPossible()
		 bool checkValid() For error handling of coords

Sudo Code:

	  Solver:
	  
		Initialize 2D array with 81 empty grids (nx = 9, ny = 9)
		Fill in some empty grid with the known values
		Make an original copy of the array
		Start from top left grid (nx = 0, ny = 0), check if grid is empty
		if (grid is empty) {
		assign the empty grid with values (i)
		if (no numbers exists in same rows & same columns same as (i) & 3x3 square (i) is currently in)
			fill in the number
		if (numbers exists in same rows & same columns same as (i) & 3x3 square (i) is currently in)
			discard (i) and repick other values (i++)
		}
		else {
		while (nx < 9) {
			Proceed to next row grid(nx++, ny)
		if (nx equals 9) {
			reset nx = 1
		proceed to next column grid(nx,ny++)
			if (ny equals 9) {
        print solution
							}
						}
					}
		}

      void interact()
      {
         char input[256];
  
         while (something is true)
      { 
         PUT Options:
            PUT ? Show these instructions
            PUT D Dislay the board
            PUT E Edit one square
            PUT S Show the possible values for a square
            PUT Q Save and quit
            PUT >
      
            GET (USER INPUT)
      }
   
         CHECK INPUT 

         switch: INPUT
            if ?     reprint options
            if D     print function displayBoard
            if E     edit square function
            if S     calc possible values
            if Q     Save board and quit
            default  ERROR: Invalid command
      }



      void computeValue(gameArray[][])
      {
         GET user input form funtion interact

         column = index % numCol;
         row = index / numCol;

         RETURN value of data at specified spot           
      }


	getting Quards from User
		
		What are the coordinates of the square: A1
		
		int main()
		{
			int row;
			char column;
			
			while 
			
		}
		
		// Pass from main will not work for extra credit!
		bool getCoordinates(int & row, char & column)
		{
			cout << "Enter: "
			
			// Asserts to check for errors
			cin >> column;
			cin >> row;
			
			if(cin.fail())
			{
				cin.clear();
				// Could work for final project clear and ignore rest of line
				cin.ignore(256, '\n');
				
				cout << "Invalid input" << endl;
				return false;
			}
			
			column = toupper(column);
			
			if (row < 1 || row > 9)
			{
				cout << "Invalid row" << endl;
				return false;
			}
		
			if (column < 'A' || column > 'I')
			{
				cout << "Invalid column" << endl;
				return false;
			}
		
			return true;
		}
